# Translations template for PROJECT.
# Copyright (C) 2022 ORGANIZATION
# This file is distributed under the same license as the PROJECT project.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2022.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PROJECT VERSION\n"
"Report-Msgid-Bugs-To: EMAIL@ADDRESS\n"
"POT-Creation-Date: 2022-10-15 20:36-0300\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Generated-By: Babel 2.8.0\n"

#: templates/base.html:59 templates/base_nomessages.html:63
#: templates/widgets/playlist.html:104
msgid "search"
msgstr ""

#: templates/base.html:87 templates/base_nomessages.html:91
msgid "Log In"
msgstr ""

#: templates/base.html:91 templates/base_nomessages.html:95
msgid "Log Out"
msgstr ""

#: templates/base.html:137 templates/donate/donate_index.html:30
msgid "Donate"
msgstr ""

#: templates/base.html:140 templates/base_nomessages.html:145
msgid "Released under the"
msgstr ""

#: templates/base.html:146 templates/base_nomessages.html:151
msgid "About"
msgstr ""

#: templates/base.html:149 templates/base_nomessages.html:154
msgid "Privacy"
msgstr ""

#: templates/base.html:152 templates/base_nomessages.html:157
msgid "FAQ"
msgstr ""

#: templates/about/about_index.html:32
msgid "limits"
msgstr ""

#: templates/about/about_index.html:33
msgid "access to videos that are neither"
msgstr ""

#: templates/about/about_index.html:35
msgid "illegal"
msgstr ""

#: templates/about/about_index.html:36
msgid "nor violate their own"
msgstr ""

#: templates/about/about_index.html:38
msgid "Terms and Conditions"
msgstr ""

#: templates/about/about_index.html:38
msgid "or"
msgstr ""

#: templates/about/about_index.html:40 templates/about/about_index.html:55
msgid "Community Guidelines"
msgstr ""

#: templates/about/about_index.html:43
msgid "\"Limited state\" videos"
msgstr ""

#: templates/about/about_index.html:44
msgid ""
"are placed behind a warning message, cannot be shared, monetized or "
"easily found, and have likes, comments, view counts and suggested videos "
"disabled"
msgstr ""

#: templates/about/about_index.html:49
msgid "removes"
msgstr ""

#: templates/about/about_index.html:50
msgid "videos that violate their"
msgstr ""

#: templates/about/about_index.html:52
msgid "ever-changing"
msgstr ""

#: templates/about/about_index.html:52
msgid "and"
msgstr ""

#: templates/about/about_index.html:54
msgid "arbitrarily enforced"
msgstr ""

#: templates/about/about_index.html:56
msgid ""
"while Creators increasingly \"self-censor\" trying to avoid channel "
"strikes and terminations"
msgstr ""

#: templates/about/about_index.html:60
msgid "is an unbiased community catalog of"
msgstr ""

#: templates/about/about_index.html:63
msgid "limited state, removed, and self-censored YouTube videos across"
msgstr ""

#: templates/about/about_index.html:66
msgid "monitored channels, of which"
msgstr ""

#: templates/about/about_index.html:69
msgid "have been deleted and"
msgstr ""

#: templates/about/about_index.html:72
msgid "are being archived in case of deletion"
msgstr ""

#: templates/about/about_index.html:75
msgid "Find videos and channels using the search bar"
msgstr ""

#: templates/about/about_index.html:76
msgid ""
"Download videos with \"right mouse button click\" inside the video window"
" or use the \"Download Torrent\" link. Channel and Video RSS Feeds"
msgstr ""

#: templates/about/about_index.html:80
msgid "contain torrent links to easily download multiple videos"
msgstr ""

#: templates/about/about_index.html:81
msgid "Embed videos in any website exactly like YouTube"
msgstr ""

#: templates/about/about_index.html:84
msgid "Email new channel suggestions to"
msgstr ""

#: templates/about/about_index.html:89
msgid "Privacy Policy"
msgstr ""

#: templates/about/about_index.html:93
msgid "Data you directly provide"
msgstr ""

#: templates/about/about_index.html:94
msgid ""
"Data that you provide to the website for the purpose of its operation "
"(e.g., an account name or password, or playlist) will be stored in the "
"websites database until the user decides to remove it. This data will not"
" be intentionally shared with anyone or anything"
msgstr ""

#: templates/about/about_index.html:96
msgid "Information stored about a registered user is limited to"
msgstr ""

#: templates/about/about_index.html:98
msgid ""
"a user ID (for persistent storage of preferences and playlists) and "
"hashed password"
msgstr ""

#: templates/about/about_index.html:99
msgid "a json object containing user preferences"
msgstr ""

#: templates/about/about_index.html:100
msgid "a list of video IDs identifying watched videos"
msgstr ""

#: templates/about/about_index.html:102
msgid "Users can clear their watch history using the "
msgstr ""

#: templates/about/about_index.html:102
msgid "clear watch history"
msgstr ""

#: templates/about/about_index.html:104
msgid "Data you passively provide"
msgstr ""

#: templates/about/about_index.html:105
msgid ""
"When you request any resource from this website (e.g., a page, font, or "
"image) information about the request may be logged"
msgstr ""

#: templates/about/about_index.html:106
msgid "Information about a request is limited to"
msgstr ""

#: templates/about/about_index.html:108
msgid "the time the request was made"
msgstr ""

#: templates/about/about_index.html:109
msgid "the status code of the response"
msgstr ""

#: templates/about/about_index.html:110
msgid "the method of the request"
msgstr ""

#: templates/about/about_index.html:111
msgid "the requested URL"
msgstr ""

#: templates/about/about_index.html:112
msgid "how long it took to complete the request"
msgstr ""

#: templates/about/about_index.html:115
msgid ""
"No identifying information is logged, such as the visitors cookie, user-"
"agent, or full IP address (only the  first /24 bits are stored for "
"geographical feedback). Here are a few lines as an example"
msgstr ""

#: templates/about/about_index.html:121
msgid ""
"This website does not store the visitors user-agent or full IP address "
"and does not use fingerprinting, advertisements, or tracking of any form"
msgstr ""

#: templates/about/about_index.html:124
msgid "Data stored in your browser"
msgstr ""

#: templates/about/about_index.html:125
msgid ""
"This website uses browser cookies to authenticate registered users. This "
"data consists of"
msgstr ""

#: templates/about/about_index.html:127
msgid ""
"An account token to keep you logged into the website between visits, "
"which is sent when any page is loaded while you are logged in"
msgstr ""

#: templates/about/about_index.html:129
msgid ""
"This website also provides an option to store site preferences (e.g., "
"theme or locale) without an account. Using this feature will store a "
"cookie in the visitors browser containing their preferences. This cookie "
"is sent on every request and does not contain any identifying information"
msgstr ""

#: templates/about/about_index.html:131
msgid ""
"You can remove this data from your browser by logging out of this "
"website, or by using your browsers cookie-related controls to delete the "
"data"
msgstr ""

#: templates/about/about_index.html:133
msgid "Removal of data"
msgstr ""

#: templates/about/about_index.html:134
msgid ""
"To remove data stored in your browser, you can log out of the website, or"
" you can use your browsers cookie-related controls to delete the data"
msgstr ""

#: templates/about/about_index.html:135
msgid ""
"To remove data that has been stored in the websites database, you can use"
" the "
msgstr ""

#: templates/about/about_index.html:136
msgid "delete my account</u></a></b> page"
msgstr ""

#: templates/about/about_index.html:140
msgid "Frequently Asked Questions"
msgstr ""

#: templates/about/about_index.html:143
msgid "How can I find videos/channels?"
msgstr ""

#: templates/about/about_index.html:144
msgid ""
"The search bar at the top of every page is the best way to locate "
"channels and videos. Use the minimum number of exact words. YouTube "
"channel and video identifiers can also be used"
msgstr ""

#: templates/about/about_index.html:151
msgid "How can I upload my videos/channels?"
msgstr ""

#: templates/about/about_index.html:152
msgid ""
"We receive channel suggestions (preferably through <b>altCensored AT "
"protonmail.com</b>) and add channels that have experienced, or are at-"
"risk of experiencing"
msgstr ""

#: templates/about/about_index.html:154
msgid "videos placed in "
msgstr ""

#: templates/about/about_index.html:156
msgid "Limited State"
msgstr ""

#: templates/about/about_index.html:156
msgid " or arbitrarily"
msgstr ""

#: templates/about/about_index.html:158
msgid "age-restricted"
msgstr ""

#: templates/about/about_index.html:158
msgid "by"
msgstr ""

#: templates/about/about_index.html:159
msgid "videos removed or demonetized"
msgstr ""

#: templates/about/about_index.html:160
msgid "channel demonetization"
msgstr ""

#: templates/about/about_index.html:163
msgid "How can I download videos/channels?"
msgstr ""

#: templates/about/about_index.html:165
msgid ""
"Download videos with \"right mouse button click\" inside the video window"
" or use the \"Download Torrent\" link"
msgstr ""

#: templates/about/about_index.html:166
msgid ""
"Channel and Video RSS Feeds contain torrent links to easily download "
"multiple videos with an RSS feed capable torrent client such as"
msgstr ""

#: templates/about/about_index.html:173
msgid "How can I embed videos?"
msgstr ""

#: templates/about/about_index.html:174
msgid ""
"Videos can be embedded (with or without iframes) using the same syntax as"
" YouTube"
msgstr ""

#: templates/admin/admin_channel_table.html:23
#: templates/admin/admin_video_table.html:25
#: templates/channel/channel_index.html:62
#: templates/channel/channel_table.html:45
msgid "Table Deleted"
msgstr ""

#: templates/admin/admin_channel_table.html:25
#: templates/admin/admin_video_table.html:27
#: templates/channel/channel_index.html:66
#: templates/channel/channel_table.html:47
msgid "Table All"
msgstr ""

#: templates/auth/auth_index.html:22 templates/auth/auth_index.html:49
#: templates/settings/settings_user_update.html:39
msgid "Email"
msgstr ""

#: templates/auth/auth_index.html:26 templates/auth/auth_index.html:27
#: templates/auth/auth_index.html:53 templates/auth/auth_index.html:54
msgid "Password"
msgstr ""

#: templates/auth/auth_index.html:30 templates/auth/auth_index.html:31
#: templates/settings/settings_user_update.html:48
msgid "Username"
msgstr ""

#: templates/auth/auth_index.html:39
msgid "Captcha"
msgstr ""

#: templates/auth/auth_index.html:44
msgid "Register"
msgstr ""

#: templates/auth/auth_index.html:45
msgid "Clear"
msgstr ""

#: templates/auth/auth_index.html:57
msgid "Sign In/Register"
msgstr ""

#: templates/auth/auth_index.html:58
msgid "Reset Password"
msgstr ""

#: templates/auth/auth_reset_password.html:16
msgid "Video"
msgstr ""

#: templates/auth/auth_reset_password.html:21
#: templates/channel/channel_item.html:67 templates/video/video_search.html:29
msgid "Channel"
msgstr ""

#: templates/auth/auth_reset_password.html:26
#: templates/video/video_item.html:75 templates/widgets/navtabs.html:72
msgid "Category"
msgstr ""

#: templates/auth/auth_reset_password.html:50
msgid "New Password"
msgstr ""

#: templates/auth/auth_reset_password.html:52
msgid "Update Pasword"
msgstr ""

#: templates/category/category_item.html:47
#: templates/channel/channel_index.html:74
#: templates/language/language_item.html:111
#: templates/language/language_item.html:125
#: templates/language/language_item.html:139
#: templates/language/language_item.html:154
#: templates/video/video_index.html:61 templates/video/video_search.html:223
#: templates/video/video_search.html:238
msgid "latest"
msgstr ""

#: templates/category/category_item.html:51
#: templates/channel/channel_index.html:78
#: templates/channel/channel_item.html:107
#: templates/language/language_item.html:114
#: templates/language/language_item.html:128
#: templates/language/language_item.html:142
#: templates/language/language_item.html:157
#: templates/playlist/playlist_index.html:42 templates/user/user_index.html:30
#: templates/user/user_playlist_index.html:37
#: templates/video/video_index.html:65 templates/video/video_search.html:227
#: templates/video/video_search.html:242
msgid "newest"
msgstr ""

#: templates/category/category_item.html:55
#: templates/channel/channel_index.html:82
#: templates/channel/channel_item.html:111
#: templates/language/language_item.html:120
#: templates/language/language_item.html:134
#: templates/language/language_item.html:148
#: templates/language/language_item.html:163
#: templates/playlist/playlist_index.html:46 templates/user/user_index.html:34
#: templates/user/user_playlist_index.html:41
#: templates/video/video_index.html:69 templates/video/video_search.html:231
#: templates/video/video_search.html:246
msgid "popular"
msgstr ""

#: templates/channel/channel_index.html:40
msgid "deleted"
msgstr ""

#: templates/channel/channel_index.html:44
msgid "most limited"
msgstr ""

#: templates/channel/channel_index.html:114
#: templates/channel/channel_item.html:46
#: templates/language/language_item.html:55
#: templates/video/video_search.html:51
msgid "Limited"
msgstr ""

#: templates/channel/channel_index.html:117
#: templates/language/language_item.html:57
#: templates/video/video_search.html:54
msgid "Total"
msgstr ""

#: templates/channel/channel_index.html:119
#: templates/channel/channel_item.html:48
#: templates/language/language_item.html:58 templates/video/video_item.html:73
#: templates/video/video_search.html:56
msgid "Views"
msgstr ""

#: templates/channel/channel_index.html:121
#: templates/language/language_item.html:59
#: templates/video/video_search.html:58
msgid "Subs"
msgstr ""

#: templates/channel/channel_index.html:128
#: templates/channel/channel_item.html:52
#: templates/language/language_item.html:67
#: templates/video/video_search.html:65
msgid "Monitor"
msgstr ""

#: templates/channel/channel_index.html:129
#: templates/channel/channel_item.html:54
#: templates/language/language_item.html:68
#: templates/language/language_item.html:72
#: templates/playlist/playlist_item_create_edit.html:49
#: templates/playlist/playlist_item_create_edit.html:51
#: templates/settings/settings_site.html:85
#: templates/settings/settings_site.html:96
#: templates/settings/settings_user_update.html:70
#: templates/settings/settings_user_update.html:81
#: templates/video/video_search.html:66
msgid "True"
msgstr ""

#: templates/channel/channel_index.html:130
#: templates/channel/channel_item.html:56
#: templates/language/language_item.html:69
#: templates/language/language_item.html:73
#: templates/playlist/playlist_item_create_edit.html:48
#: templates/playlist/playlist_item_create_edit.html:52
#: templates/settings/settings_site.html:83
#: templates/settings/settings_site.html:94
#: templates/settings/settings_user_update.html:68
#: templates/settings/settings_user_update.html:79
#: templates/video/video_search.html:67
msgid "False"
msgstr ""

#: templates/channel/channel_index.html:132
#: templates/channel/channel_item.html:58
#: templates/language/language_item.html:71
#: templates/video/video_search.html:69
msgid "Archive"
msgstr ""

#: templates/channel/channel_index.html:133
#: templates/channel/channel_item.html:60 templates/video/video_search.html:70
msgid "Full"
msgstr ""

#: templates/channel/channel_index.html:134
#: templates/video/video_search.html:71
msgid "Part"
msgstr ""

#: templates/channel/channel_index.html:135
#: templates/channel/channel_item.html:64 templates/video/video_search.html:72
msgid "None"
msgstr ""

#: templates/channel/channel_index.html:145
#: templates/channel/channel_item.html:70
#: templates/language/language_item.html:83
#: templates/video/video_search.html:83
msgid "Created"
msgstr ""

#: templates/channel/channel_index.html:148
#: templates/channel/channel_item.html:73 templates/video/video_search.html:86
msgid "Deleted"
msgstr ""

#: templates/channel/channel_item.html:47
msgid "Videos"
msgstr ""

#: templates/channel/channel_item.html:49
msgid "Subscribers"
msgstr ""

#: templates/channel/channel_item.html:62
msgid "Partial"
msgstr ""

#: templates/channel/channel_item.html:68
msgid "Stats"
msgstr ""

#: templates/donate/donate_index.html:32
#, python-format
msgid ""
"We receive no advertising or corporate revenue and are 100%% community "
"funded:"
msgstr ""

#: templates/donate/donate_index.html:34
msgid "\"together we show what they hide\""
msgstr ""

#: templates/donate/donate_index.html:37
msgid "Credit Cards"
msgstr ""

#: templates/donate/donate_index.html:42
msgid "Cryptocurrency"
msgstr ""

#: templates/donate/donate_index.html:60
msgid ""
"Thank-you Brave Browser/BAT users, your generous support makes a "
"difference"
msgstr ""

#: templates/donate/donate_index.html:63
msgid "Request additional cryptocurrency addresses via email to"
msgstr ""

#: templates/language/language_item.html:117
#: templates/language/language_item.html:131
#: templates/language/language_item.html:145
#: templates/language/language_item.html:160
msgid "oldest"
msgstr ""

#: templates/language/language_item.html:199 templates/widgets/videos.html:91
msgid "Published"
msgstr ""

#: templates/language/language_item.html:202
#: templates/video/video_item.html:176 templates/widgets/videos.html:94
msgid "views"
msgstr ""

#: templates/playlist/playlist_item_create_edit.html:19
#: templates/settings/settings_index.html:54
#: templates/settings/settings_site.html:52
#: templates/user/user_playlist_index.html:20
#: templates/video/video_search.html:109 templates/vpn/vpn_index.html:46
msgid "Playlist"
msgstr ""

#: templates/playlist/playlist_item_create_edit.html:29
#: templates/playlist/playlist_item_create_edit.html:33
msgid "Title"
msgstr ""

#: templates/playlist/playlist_item_create_edit.html:37
#: templates/playlist/playlist_item_create_edit.html:41
#: templates/settings/settings_user_update.html:56
msgid "Description"
msgstr ""

#: templates/playlist/playlist_item_create_edit.html:45
#: templates/user/user_item.html:99 templates/user/user_playlist_index.html:79
#: templates/widgets/playlist.html:63
msgid "Public"
msgstr ""

#: templates/playlist/playlist_item_create_edit.html:58
msgid "Update Playlist"
msgstr ""

#: templates/playlist/playlist_item_create_edit.html:60
msgid "Create Playlist"
msgstr ""

#: templates/settings/settings_index.html:32
#: templates/settings/settings_site.html:30 templates/vpn/vpn_index.html:24
msgid "Site"
msgstr ""

#: templates/settings/settings_index.html:39
#: templates/settings/settings_site.html:37 templates/vpn/vpn_index.html:31
msgid "VPN"
msgstr ""

#: templates/settings/settings_index.html:47
#: templates/settings/settings_site.html:45
#: templates/video/video_search.html:156 templates/vpn/vpn_index.html:39
#: templates/widgets/navtabs.html:56
msgid "User"
msgstr ""

#: templates/settings/settings_site.html:61
msgid "Language"
msgstr ""

#: templates/settings/settings_site.html:70
msgid "Theme"
msgstr ""

#: templates/settings/settings_site.html:79
msgid "AutoPlay"
msgstr ""

#: templates/settings/settings_site.html:90
msgid "Loop List"
msgstr ""

#: templates/settings/settings_site.html:101
msgid "Menu"
msgstr ""

#: templates/settings/settings_site.html:122
msgid "Save"
msgstr ""

#: templates/settings/settings_user_update.html:64
msgid "User Public"
msgstr ""

#: templates/settings/settings_user_update.html:75
msgid "Email Subscribed"
msgstr ""

#: templates/settings/settings_user_update.html:86
msgid "Featured Playlist"
msgstr ""

#: templates/settings/settings_user_update.html:91
#: templates/video/video_item.html:88
msgid "Select Playlist"
msgstr ""

#: templates/settings/settings_user_update.html:99
msgid "Update User"
msgstr ""

#: templates/user/user_history_index.html:32
msgid "history"
msgstr ""

#: templates/user/user_item.html:73 templates/widgets/playlist.html:92
msgid "History"
msgstr ""

#: templates/user/user_item.html:84
#: templates/user/user_watchlater_index.html:32
#: templates/video/video_item.html:93 templates/widgets/playlist.html:98
msgid "WatchLater"
msgstr ""

#: templates/user/user_item.html:102 templates/user/user_playlist_index.html:82
msgid "Private"
msgstr ""

#: templates/video/video_item.html:66
msgid "View on"
msgstr ""

#: templates/video/video_item.html:70
msgid "Download Torrent"
msgstr ""

#: templates/video/video_item.html:99 templates/widgets/videos.html:31
msgid "Add to Playlist"
msgstr ""

#: templates/video/video_item.html:135
msgid "Published on"
msgstr ""

#: templates/video/video_item.html:150
msgid "AutoPlay Next Video"
msgstr ""

#: templates/vpn/vpn_index.html:53
msgid "Connections"
msgstr ""

#: templates/vpn/vpn_index.html:86
msgid "Download"
msgstr ""

#: templates/vpn/vpn_index.html:102
msgid "Nodes"
msgstr ""

#: templates/vpn/vpn_index.html:113
msgid "Create New Connection"
msgstr ""

#: templates/vpn/vpn_index.html:121
msgid "Why use the altCensored VPN?"
msgstr ""

#: templates/vpn/vpn_index.html:123
msgid ""
"A virtual private network (VPN) service provides a proxy server to help "
"users bypass Internet censorship such as                     geoblocking "
"and users who want to protect their communications against data profiling"
" or MitM attacks on hostile networks"
msgstr ""

#: templates/vpn/vpn_index.html:127
msgid "Our goal is to raise funds for fighting censorship"
msgstr ""

#: templates/vpn/vpn_index.html:128
msgid "(together we show what they hide)"
msgstr ""

#: templates/vpn/vpn_index.html:129
msgid "and improve online privacy"
msgstr ""

#: templates/vpn/vpn_index.html:130
msgid "(together we hide what they want)"
msgstr ""

#: templates/vpn/vpn_index.html:137
msgid "Wireguard Client Setup"
msgstr ""

#: templates/vpn/vpn_index.html:139
msgid "Official"
msgstr ""

#: templates/vpn/vpn_index.html:140
msgid "Wireguard Clients"
msgstr ""

#: templates/widgets/navtabs.html:40
msgid "Settings"
msgstr ""

#: templates/widgets/navtabs.html:88
msgid "Admin"
msgstr ""

#: templates/widgets/videos.html:44 templates/widgets/videos.html:53
#: templates/widgets/videos.html:68
msgid "Add to WatchLater"
msgstr ""

#: templates/widgets/widgets_confirm.html:22
msgid "Yes"
msgstr ""

#: templates/widgets/widgets_confirm.html:23
msgid "No"
msgstr ""

